package com.example.room;

import android.app.Application;
import android.util.Log;
import androidx.lifecycle.AndroidViewModel;

public class NoteView extends AndroidViewModel {

    private String TAG = this.getClass().getSimpleName();

    public NoteView(Application application){
        super(application);
    }

    protected void onCleared(){
        super.onCleared();
        Log.i(TAG,"NoteView Destroyed");
    }

}